final class LazyFinal<T>
{
	private T t;

	public boolean isSet() {
		if (this.t != null) {
			return true;
		} else {
			return false;
		}
	}

	public T getT() {
		return this.t;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result * t.hashCode();
		return result ;
	}

	@Override
	public boolean equals(Object comparedObject) {
		if (comparedObject == null) {
			return false;
		}
		
		if (this.getClass() != comparedObject.getClass()) {
			return false;
		}
		
		if (this.t != ((LazyFinal<Object>)comparedObject).getT()) {
			return false;
		}
		
		if (!this.t.equals(((LazyFinal<Object>)comparedObject).getT())) {
			return false;
		}
		
		return true;
	}

	public String toString() {
		return String.valueOf(this.t);
	}

	public static void main(String args) {
		LazyFinal lazyFinal = new LazyFinal<Object>();
		System.out.println(lazyFinal.toString());
	}
}
